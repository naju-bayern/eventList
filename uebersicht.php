<?php
require("eventListFunctios.php");
?>
<h3>Arbeitskreise</h3>
<?php printEvents(array("ak"),0,true); ?>

<h3>Seminare & Freizeiten für Kinder</h3>
<?php printEvents(array("kinderseminar"),0,true); ?>

<h3>Seminare & Freizeiten für Jugendliche</h3>
<?php printEvents(array("jugendseminar"),0,true); ?>

<h3>Vogelschutzcamps (bis 16 Jahre)</h3>
<?php printEvents(array("orniseminar"),0,true); ?>

<h3>Streif&uuml;ge (ab 16 Jahre)</h3>
<?php printEvents(array("streifzug"),0,true); ?>

<h3>Aus- und Weiterbildung für Gruppenleiter und Interessierte</h3>
<?php printEvents(array("multiseminar"),0,true); ?>

<h3>Bundesnaju</h3>
<?php printEvents(array("bundes"),0,true); ?>

<h3>LBV</h3>
<?php printEvents(array("lbv"),0,true); ?>

<h3>Demos</h3>
<?php printEvents(array("demo"),0,true); ?>
