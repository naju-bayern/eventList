# NAJU EventList

Hier findet mann die Templates für die EventListFactory (https://gitlab.com/jakobbraun/eventListFactory)

## Verwendung mit office365:
[Zeitraum auf ein Jahr erweitern](https://answers.microsoft.com/en-us/msoffice/forum/msoffice_outlook-mso_other/how-to-get-published-office365-calendar-to-have/45d8c2f7-9aa0-4321-8839-3e4abf3698bb)

**Achtung:** 
`Kalender` statt `Calendar` verwenden.
```
    Set-MailboxCalendarFolder -Identity **EMAIL**:\Kalender\najuWsNew –PublishEnabled $true -PublishDateRangeFrom SixMonths -PublishDateRangeTo OneYear
```